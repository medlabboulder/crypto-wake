# Crypto Wake

**This project has been deprecated; the content is now hosted under [Stories in the Zodiac wiki](https://zodiac.wiki/index.php/Category:Stories)**.

## How to contribute

The Projects files are located in `files/docs/projects/`. See more details about how to contribute on the [Contributing page](https://cryptowake.xyz/about/contributing/).

## Models

* [Civic Tech Graveyard](https://civictech.guide/graveyard/)
* [DOTCOM SÉANCE](https://www.dotcomseance.com/)
* [ZODIAC.WIKI](https://zodiac.wiki/)

## Open questions

- Should this be migrated to Jeykll with a csv backend? Or a Google doc?
    - Would be easier to maintain the database and uniform layout
